//
//  Store.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 27/04/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

import Foundation

public struct AllStoresStore: PlainObject
{
    public let id: Int32
    public let logo_url: String?
    public let name: String?
    
    public init(id: Int32, name: String?, logo_url: String?)
    {
        self.id = id
        self.logo_url = logo_url
        self.name = name
    }
}

extension AllStoresStore
{
    public static func plainFromJSON(json:JSON) -> AllStoresStore {
        
        return AllStoresStore(
            id: json["id"].int32!,
            name: json["name"].string == nil ? nil : json["name"].string!.trimmingCharacters(in: .whitespacesAndNewlines),
            logo_url: json["logo_url"].string
        )
    }
}

extension AllStoresStore
{
    public typealias CoreDataType = AllStoresEntity
    
    public func mapToEntity(entity: AllStoresEntity)
    {
        entity.id = id
        entity.name = name
        entity.logo_url = logo_url
    }
}

