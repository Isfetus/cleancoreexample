//
//  AbstractStore+CoreDataProperties.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 12/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData


extension AbstractStore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AbstractStore> {
        return NSFetchRequest<AbstractStore>(entityName: "AbstractStore")
    }

    @NSManaged public var id: Int32
    @NSManaged public var logo_url: String?
    @NSManaged public var name: String?

}
