//
//  AllStoresEntity+Extension.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 27/04/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

extension AllStoresEntity: PlainConvertibleType
{
    public func asPlain() -> AllStoresStore
    {
        return AllStoresStore(id: id, name: name, logo_url: logo_url)
    }
}

extension AllStoresEntity: Persistable
{
    public static var entityName: String
    {
        return "AllStoresEntity"
    }
}


