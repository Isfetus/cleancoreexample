//
//  AppDelegate.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 11/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    let appAssembler = AppAssembler()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        window.makeKeyAndVisible()
        self.window = window
        
        let mainViewController = appAssembler.mainViewController()

        window.rootViewController = mainViewController
        
        return true
    }
}

