//
//  AppAssembler.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 12/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let coreDataStackAllStores = "CoreDataStackAllStores"

class AppAssembler
{
    init()
    {
        Container.loggingFunction = nil
    }
    
    fileprivate let assembler = Assembler([AllStoresAssembly()], container: SwinjectStoryboard.defaultContainer)
}

extension AppAssembler
{
    func mainViewController() -> UIViewController
    {
        let mainVC = SwinjectStoryboard.create(name: "Main", bundle: nil).instantiateInitialViewController()
        return mainVC!
    }
}
