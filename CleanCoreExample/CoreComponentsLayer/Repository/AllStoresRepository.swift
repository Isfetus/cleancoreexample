//
//  AllStoresRepository.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 12/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class AllStoresRepository: ListRepository<AllStoresStore, AllStoresEntity> {}

