//
//  AllStoresWrapper.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 10/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

struct AllStoresResponseMapper: ResponseMapper
{
    public var response:Any
    
    static public func parse(responseJSON:JSON) -> Any
    {
        return responseJSON["response"]["stores_list"].arrayValue.map { (json:JSON) -> AllStoresStore in
            
            return AllStoresStore.plainFromJSON(json: json)
        }
    }
}
