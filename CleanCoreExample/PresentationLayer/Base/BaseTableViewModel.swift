//
//  BaseTableViewViewModel.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 15/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class BaseTableViewModel<VIEW_MODEL_TYPE, RESULT:Sequence, ENTITY>: BaseViewModel<RESULT, ENTITY>
{
    public var sections = Variable<[SectionModel<String, VIEW_MODEL_TYPE>]>([SectionModel<String, VIEW_MODEL_TYPE>]())
    
    public func indexBarTitles () -> [String]
    {
        return self.sections.value.map { (section:SectionModel<String, VIEW_MODEL_TYPE>) -> String in
            
            return section.model
        }
    }
    
    internal func parseModels(response: RESULT)
    {
        assert(false, "BaseTableViewViewModel: parseModels method haven't been overrided")
    }
    
    internal func getCellViewModel(store:RESULT.Iterator.Element) -> VIEW_MODEL_TYPE
    {
        assert(false, "BaseTableViewViewModel: getCellViewModel method haven't been overrided")
    }
    
    internal func makeSectionsDataForTableView<VIEW_MODEL_TYPE>(sectionNames:[String], sectionItems:[String:[VIEW_MODEL_TYPE]]) -> [SectionModel<String, VIEW_MODEL_TYPE>]
    {
        var sectionModels = [SectionModel<String, VIEW_MODEL_TYPE>]()
        
        for sectionName in sectionNames
        {
            sectionModels.append(SectionModel(model: sectionName, items: sectionItems[sectionName]!))
        }
        
        return sectionModels
    }
}
