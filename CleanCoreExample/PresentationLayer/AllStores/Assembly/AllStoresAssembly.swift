//
//  AllStoresAssembly.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 12/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let coreDataStackAllStores = "CoreDataStackStore"

class AllStoresAssembly: Assembly {
    
    func assemble(container: Container) {
        
        // Interactor

        container.register(Network.self) { _ in Network() }
        container.register(StoresCoreDataStack.self, name:coreDataStackAllStores) { _ in StoresCoreDataStack() }
        
        container.register(AllStoresRepository.self) { r in  AllStoresRepository(context: (container.resolve(StoresCoreDataStack.self, name:coreDataStackAllStores)?.context)!)}
        
        container.register(BaseStorageService<[AllStoresStore], AllStoresEntity>.self) { r in
            
            BaseStorageService<[AllStoresStore], AllStoresEntity>(repository: container.resolve(AllStoresRepository.self)!,
                                                                       networking: container.resolve(Network.self)!,
                                                                       responseMapper:AllStoresResponseMapper.self)
        }
                
        container.register(StoresInteractor.self) { r in StoresInteractor(service: r.resolve(BaseStorageService<[AllStoresStore], AllStoresEntity>.self)!)}
        
        // ViewModel
        
        container.register(StoresViewModel.self) { r in StoresViewModel(interactor: r.resolve(StoresInteractor.self)!)}
        
        // View
        
        container.storyboardInitCompleted(StoresViewController.self) { r, c in
            c.viewModel = r.resolve(StoresViewModel.self)!
        }
    }
}
