//
//  StoresInteractor.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 13.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class StoresInteractor: BaseStorageInteractor<[AllStoresStore]>
{
    override open func getData(params:RequestParameters.Type) -> Observable<[AllStoresStore]>
    {
        return self.service.getData(params: params)
    }
}
