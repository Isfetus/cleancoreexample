//
//  StoreCellViewModel.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 17.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift

class StoreCellViewModel: NSObject
{
    private var network:Networking
    
    public var id = Variable<Int>(0)
    public var name = Variable<String?>(nil)
    public var logoUrl = Variable<String?>(nil)
    public var cashback = Variable<String?>(nil)
    
    init(network: Networking)
    {
        self.network = network
        
        super .init()
    }
    
    public func setId(id: Int)
    {
        self.id.value = id
    }
    
    public func setName(name:String?)
    {
        self.name.value = name
    }
    
    public func setLogoUrl(logoUrl:String?)
    {
        self.logoUrl.value = logoUrl
    }
    
    public func setCashback(cashback:String?)
    {
        self.cashback.value = cashback
    }
    
    public func getLogoImage() -> Observable<UIImage>
    {
        if let logoUrl = self.logoUrl.value
        {
            return self.network.requestImage(logoUrl)
        }
        else
        {
            return Observable<UIImage>.error(RxError.noElements)
        }
    }
}
