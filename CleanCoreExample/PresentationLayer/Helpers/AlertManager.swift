//
//  AlertManager.swift
//  ClearCoreExample
//
//  Created by Andrey Belkin on 30.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let BUTTONS_TEXT_COLOR = UIColor(netHex: 0x6ea627)

class AlertManager: NSObject {

    static let shared = AlertManager()
 
    func makeWindowAndPresentAlertController(alertController:UIAlertController)
    {
        let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController!.present(alertController, animated: true, completion: nil)
    }
    
    func showConfirm(title:String?, message: String?, actions: [String: (() -> Void)])
    {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = BUTTONS_TEXT_COLOR
        
        for (buttonName, buttonAction) in actions
        {
            alertController.addAction(UIAlertAction(title: buttonName, style: .default, handler: { (action:UIAlertAction) in
                
                buttonAction()
            }))
        }
        
        makeWindowAndPresentAlertController(alertController: alertController)
    }
}
